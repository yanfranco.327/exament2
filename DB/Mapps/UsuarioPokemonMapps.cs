﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExamenT2_Diars.Models;

namespace ExamenT2_Diars.DB.Mapps
{
    public class UsuarioPokemonMapps : IEntityTypeConfiguration<UsuarioPokemon>
    {
        public void Configure(EntityTypeBuilder<UsuarioPokemon> builder)
        {
            builder.ToTable("UsuarioPokemon");
            builder.HasKey(o => new { o.PokemonId, o.UsuarioId });
            builder.HasOne(o => o.Usuario).WithMany(o => o.PokemonesCapturados).HasForeignKey(o => o.UsuarioId);
            builder.HasOne(o => o.Pokemon).WithMany(o => o.Usuarios).HasForeignKey(o => o.PokemonId);
        }
    }
}
