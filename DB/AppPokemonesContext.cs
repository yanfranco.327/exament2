﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ExamenT2_Diars.Models;
using ExamenT2_Diars.DB.Mapps;



namespace ExamenT2_Diars.DB
{
    public class AppPokemonesContext : DbContext
    {
        public DbSet<Tipo> Tipos { get; set; }
        public DbSet<Pokemon> Pokemons { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<UsuarioPokemon> UsuarioPokemons { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server = (local)\\SQLExpress; Database = T2; User Id = Diars; Password = Diars2021; Trusted_Connection = False; MultipleActiveResultSets = True");

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new PokemonMapps());
            modelBuilder.ApplyConfiguration(new TipoMapps());
            modelBuilder.ApplyConfiguration(new UsuarioMapps());
            modelBuilder.ApplyConfiguration(new UsuarioPokemonMapps());
        }
    }
}
