﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ExamenT2_Diars.DB;
using ExamenT2_Diars.Models;

namespace ExamenT2_Diars.Controllers
{
    public class AuthController : Controller
    {
        private readonly AppPokemonesContext context;

        public AuthController()
        {
            context = new AppPokemonesContext();
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(Usuario usuario)
        {
            if (!string.IsNullOrEmpty(usuario.Username))
            {
                var usuarioEncontrado = context.Usuarios.Where(o => o.Username == usuario.Username && o.Password == usuario.Password).FirstOrDefault();
                if (usuarioEncontrado != null)
                {
                    HttpContext.Session.SetString("UsuarioId", usuarioEncontrado.Id.ToString());
                    return RedirectToAction("Index", "Pokemon");
                }
            }
            ViewBag.LoginError = "Datos erroneos";
            return View();
        }
    }
}
