﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using ExamenT2_Diars.DB;
using ExamenT2_Diars.Models;

namespace ExamenT2_Diars.Controllers
{

    public class PokemonController : Controller
    {
        private readonly AppPokemonesContext context;
        public IHostEnvironment _hostEnv;

        public PokemonController(IHostEnvironment hostEnv)
        {
            context = new AppPokemonesContext();
            _hostEnv = hostEnv;
        }
        public IActionResult Index(string nombre = "")
        {
            var pokemons = context.Pokemons.Include(o => o.Tipo).AsQueryable();
            if (!string.IsNullOrEmpty(nombre))
            {
                pokemons = pokemons.Where(o => o.Nombre.Contains(nombre));
            }
            ViewBag.Busqueda = nombre;
            ViewBag.UsuarioId = HttpContext.Session.GetString("UsuarioId");
            return View(pokemons.ToList());
        }

        [HttpGet]
        public IActionResult Registrar()
        {
            var tipos = context.Tipos.ToList();
            ViewBag.UsuarioId = HttpContext.Session.GetString("UsuarioId");
            ViewBag.Tipos = tipos;
            return View(new Pokemon());
        }

        [HttpPost]
        public IActionResult Registrar(Pokemon pokemon, IFormFile image)
        {
            if (EsValido(pokemon) && image != null)
            {
                pokemon.Imagen = SaveImage(image);
                context.Pokemons.Add(pokemon);
                context.SaveChanges();
                ViewBag.UsuarioId = HttpContext.Session.GetString("UsuarioId");
                return RedirectToAction("Index");
            }

            var tipos = context.Tipos.ToList();
            ViewBag.Tipos = tipos;

            if (image == null)
            {
                ModelState.AddModelError("Imagen", "La imagen es requerida");
            }

            return View(pokemon);
        }

        [HttpPost]
        public IActionResult Capturar(UsuarioPokemon usuarioPokemon)
        {
            var relacion = context.UsuarioPokemons.Where(o => o.UsuarioId == usuarioPokemon.UsuarioId && o.PokemonId == usuarioPokemon.PokemonId).FirstOrDefault();
            if (relacion == null)
            {
                usuarioPokemon.Fecha = DateTime.Now;
                context.UsuarioPokemons.Add(usuarioPokemon);
                context.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Liberar(UsuarioPokemon usuarioPokemon)
        {
            var relation = context.UsuarioPokemons.Where(o => o.PokemonId == usuarioPokemon.PokemonId && o.UsuarioId == usuarioPokemon.UsuarioId).FirstOrDefault();
            context.UsuarioPokemons.Remove(relation);
            context.SaveChanges();
            return RedirectToAction("Capturados");
        }

        public IActionResult Capturados()
        {
            ViewBag.UsuarioId = HttpContext.Session.GetString("UsuarioId");
            var usuarioId = HttpContext.Session.GetString("UsuarioId");
            var pokemonesCapturados = context.UsuarioPokemons.Where(o => o.UsuarioId.ToString() == usuarioId).Include(o => o.Pokemon).ThenInclude(o => o.Tipo).ToList();
            return View(pokemonesCapturados);
        }

        private string SaveImage(IFormFile image)
        {
            if (image != null && image.Length > 0)
            {
                var basePath = _hostEnv.ContentRootPath + @"\wwwroot";
                var ruta = @"\pokemones\" + image.FileName;

                using (var strem = new FileStream(basePath + ruta, FileMode.Create))
                {
                    image.CopyTo(strem);
                    return ruta;
                }
            }
            return null;
        }

        public bool EsValido(Pokemon pokemon)
        {

            if (string.IsNullOrEmpty(pokemon.Nombre))
            {
                return false;
            }

            var encontrado = context.Pokemons.Where(o => o.Nombre == pokemon.Nombre).FirstOrDefault();
            if (encontrado != null)
            {
                ModelState.AddModelError("Nombre", "El nombre ya está registrado");
                return false;
            }
            return true;
        }
    }
}


