﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ExamenT2_Diars.DB;
using ExamenT2_Diars.Models;


namespace ExamenT2_Diars.Controllers
{
    public class TipoController : Controller
    {
        private readonly AppPokemonesContext context;

        public TipoController()
        {
            context = new AppPokemonesContext();
        }

        public IActionResult Index()
        {
            var tipos = context.Tipos.ToList();
            return View(tipos);
        }


        public IActionResult Registrar()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Registrar(Tipo tipo)
        {
            context.Tipos.Add(tipo);
            context.SaveChanges();
            return RedirectToAction("Index");
        }


    }
}
