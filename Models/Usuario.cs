﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenT2_Diars.Models
{
    public class Usuario {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public List<UsuarioPokemon> PokemonesCapturados { get; set; }
    }
}
