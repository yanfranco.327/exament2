﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenT2_Diars.Models
{
    public class Tipo{
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}

