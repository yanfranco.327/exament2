﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenT2_Diars.Models
{
    public class Pokemon
    {
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }

        [Required]
        [Display(Name = "Tipo")]
        public int TipoId { get; set; }
    
        public string Imagen { get; set; }
        public Tipo Tipo { get; set; }
        public List<UsuarioPokemon> Usuarios { get; set; }
    }
}
